# Java Principle : Naming a class
**To read**: [https://codingstories.io/story/646c5d401546c8290a2e4f5a/https:%2F%2Fgitlab.com%2Fsagarthapa335%2Ffirststory/editor]

**Estimated reading time**: 10 min

## Story Outline
Hopefully, now you better understand importance of naming correctly and how to achieve it in practice. 

## Story Organization
**Story Branch**: main
> `git checkout main`

Tags: #java